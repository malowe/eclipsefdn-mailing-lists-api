clean:;
	mvn clean
compile-java: validate-spec;
	mvn compile package
compile-java-quick: validate-spec;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
install-npm:;
	npm ci
validate-spec: install-npm;
	npm run test
compile-start: compile-quick;
	docker-compose down
	docker-compose build
	docker-compose up
start-spec: validate-spec;
	npm run start