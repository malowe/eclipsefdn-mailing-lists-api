CREATE TABLE `mailing_list_subscriptions` (
  `email` varchar(254) NOT NULL DEFAULT '',
  `list_name` varchar(254) NOT NULL DEFAULT '',
  `digest` tinyint(1) DEFAULT 0,
  `subscribed` datetime NOT NULL,
  UNIQUE KEY `email` (`email`(125),`list_name`(125))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `mailing_lists` (
  `list_name` varchar(255) NOT NULL DEFAULT '',
  `list_description` text DEFAULT NULL,
  `is_private` tinyint(1) DEFAULT 1,
  `project_id` varchar(100) NOT NULL,
  `list_short_description` varchar(255) DEFAULT NULL,
  `is_for_news_archives` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `create_archives` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `is_disabled` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `is_subscribable` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `create_date` datetime DEFAULT NULL,
  `created_by` char(20) DEFAULT NULL,
  `provision_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`list_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;